#!/bin/python3
from wiki import build_file
import os

for file in os.listdir('content/'):
    if file != '.git':
        build_file(file.replace('.md', ''))
        print(f'{file} has been made static.')
