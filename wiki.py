#!/bin/python3
import jinja2, cgi, git, markdown, os, html
from markdown.extensions.wikilinks import WikiLinkExtension

def to_html(text):
    return markdown.markdown(text, extensions=['tables', 'fenced_code', WikiLinkExtension(base_url='/', end_url='.html')])

repo = git.Repo('content')
loader = jinja2.FileSystemLoader(searchpath="./")
env = jinja2.Environment(loader=loader)
r = cgi.FieldStorage()
username = os.environ['REMOTE_USER']

def sopen(directory, file, mode='r'):
    safe_path = f'{os.getcwd()}/{directory}/'
    file_path = f'{directory}/{file}'
    if os.path.commonprefix((os.path.realpath(file_path), safe_path)) == safe_path:
        return open(file_path, mode)
    else:
        return None

def build_file(name):
    md = sopen('content', f"{name}.md").read()
    template = env.get_template('page.html')
    html = template.render(name=name, content=to_html(md))
    sopen('public', f"{name}.html", 'w').write(html)

if r.getvalue('edit'):
    name = r.getvalue('edit')
    try:
        md = sopen('content', f'{name}.md').read()
    except FileNotFoundError:
        md = ""
    template = env.get_template('editor.html')
    final = template.render(name=name, content=md.replace('</textarea>', html.escape('</textarea>')))

elif r.getvalue('submit'):
    name = r.getvalue('submit')
    content = r.getvalue('content')
    summary = r.getvalue('summary')
    if content == None:
        content = "Page deleted"
    sopen('content', f'{name}.md', 'w').write(content)
    repo.index.add(f'{name}.md')
    if summary == None:
        summary = "No summary provided"
    author = git.objects.util.Actor(username, 'anon@email')
    commit = repo.index.commit(summary, author=author)
    build_file(name)
    final = f'<meta http-equiv="refresh" content="0;url=\'/{name}.html\'" />'

elif r.getvalue('log'):
    name = r.getvalue('log')
    log = repo.iter_commits(paths=f'{name}')
    template = env.get_template('log.html')
    final = template.render(log=log, name=name.replace('.md', ''))

elif r.getvalue('diff'):
    commits = list(repo.iter_commits())
    id = len(commits) - int(r.getvalue('diff'))
    diff = repo.git.diff(commits[id+1], commits[id])
    template = env.get_template('log.html')
    final = template.render(log=commits, name='.', diff=diff)

else:
    final = '<meta http-equiv="refresh" content="0;url=\'?name=index\'" />'

print('Content-type: text/html\n')
print(final)
